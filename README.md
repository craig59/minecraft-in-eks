# Minecraft in EKS

This project describes the architecture and deployment methodology for running Minecraft in EKS

  deployment and service
  configmap for whitelisting players
  service is of type LoadBalancer to expose to internet
  Access is restricted to the minecraft server by applying a loadBalancerSourceRanges on the service for the specific source IPs needing access.
  By doing this, a security group is applied to the application load balancer limiting access.
  Access can also be restriced by supplying a whitelist of users as a configuration map

# AWS Setup

An IAM user is added with enough permissions to EKS to handle the deployment.

Create an access key

# AWS EKS Environment

AWS EKS created using Terraform
  - VPC with subnets in 3 AZs ( 3 private subnets and 3 public subnets )
Provision a Bastion host in one of the public subnets
  - Use ansible to configure the host with necessary tools for managing EKS including kubctl

Use output of terraform and populate Hashicorp vault with kubeconfig

# Gitlab CI/CD

Gitlab project contains the yaml files for deploying the Minecraft application into EKS

Gitlab CI/CD utilizes docker containers in the deployment pipeline.  The pipelines are defined by a .gitlab-ci.yml file in the root of the project.

## Deployment

During the deployment phase of the CI/CD pipeline, the amazon/aws-cli container is used since it contains the AWS command line tool needed for interacting with AWS.  Steps of the deployment:

- download kubectl
- Retrieve secrets from Hashicorp vault and store in variables.
  Secrets to retrive are:
    AWS Access key ID
    AWS Secret Access key
    kubeconfig
- Configure AWS CLI using AWS Access key ID, AWS Secret Access key and default region.  
- Create kubeconfig yaml file from the secret retrieved above
- Update kubeconfig for EKS using the aws eks command
- Deploy the minecraft yaml files

## Teardown

Teardown is performed by running a separate pipeline in Gitlab which can be invoked by adding a variable of "action=teardown". This will perform a full teardown of the minecraft application in EKS.

# Self service

Invite your friend to the Gitlab  project as a developer so she will have the ability to deploy and teardown the minecraft server.

Optionally give the friend a webhook that can be called to invoke the deploy or teardown.

# Alerting

Set up AWS Container Insights on the cluster which provides cluster and pod metrics to Cloud Watch.  From CloudWatch set up a CloudWatch alarm based on the pod_number_of_container_restarts metric.  From the CloudWatch alarm, you can send a SNS notification as a SMS text message to someones phone.
